package com.example.minu.movieapp.model;

import java.util.List;

/**
 * Created by minu on 10/12/2017.
 */

public class GetUpcoming {


    /**
     * results : [{"vote_count":555,"id":335984,"video":false,"vote_average":7.8,"title":"Blade Runner 2049","popularity":379.475381,"poster_path":"/cbRQVCia0urtv5UGsVFTdqLDIRv.jpg","original_language":"en","original_title":"Blade Runner 2049","genre_ids":[28,9648,878,53],"backdrop_path":"/mVr0UiqyltcfqxbAUcLl9zWL8ah.jpg","adult":false,"overview":"Thirty years after the events of the first film, a new blade runner, LAPD Officer K, unearths a long-buried secret that has the potential to plunge what's left of society into chaos. K's discovery leads him on a quest to find Rick Deckard, a former LAPD blade runner who has been missing for 30 years.","release_date":"2017-10-04"},{"vote_count":438,"id":381283,"video":false,"vote_average":7,"title":"mother!","popularity":129.572806,"poster_path":"/qmi2dsuoyzZdJ0WFZYQazbX8ILj.jpg","original_language":"en","original_title":"mother!","genre_ids":[18,27],"backdrop_path":"/uuQpQ8VDOtVL2IO4y2pR58odkS5.jpg","adult":false,"overview":"A couple's relationship is tested when uninvited guests arrive at their home, disrupting their tranquil existence.","release_date":"2017-09-13"},{"vote_count":211,"id":415842,"video":false,"vote_average":5.1,"title":"American Assassin","popularity":102.656643,"poster_path":"/o40BAqdTQHiN3cUfpgieDUYI71z.jpg","original_language":"en","original_title":"American Assassin","genre_ids":[28,53],"backdrop_path":"/puKZWmBIpuEMwGCn2hZkublG1rO.jpg","adult":false,"overview":"Following the murder of his fiancée, Mitch Rapp trains under the instruction of Cold War has-been Stan Hurley. The pair then is enlisted to investigate a wave of apparently random attacks on military and civilian targets.","release_date":"2017-09-14"},{"vote_count":4,"id":440021,"video":false,"vote_average":8,"title":"Happy Death Day","popularity":62.139432,"poster_path":"/FFm2DNpzfPDvIy8DKSyZ71et8W.jpg","original_language":"en","original_title":"Happy Death Day","genre_ids":[27,9648,53],"backdrop_path":"/g8RPRNaXqF456tEo2PA6ejrD8Bh.jpg","adult":false,"overview":"A college student relives the day of her murder over and over again as she tries to discover her killer's identity.","release_date":"2017-10-12"},{"vote_count":6,"id":372343,"video":false,"vote_average":4.6,"title":"The Snowman","popularity":61.558354,"poster_path":"/bQHgpTVsAWjNQWS0frsl7DlzLX1.jpg","original_language":"en","original_title":"The Snowman","genre_ids":[80,18,27,9648,53],"backdrop_path":"/tAg6KUBANIVbYUpTHy5oKibhhw3.jpg","adult":false,"overview":"Detective Harry Hole investigates the disappearance of a woman whose pink scarf is found wrapped around an ominous looking snowman.","release_date":"2017-10-12"},{"vote_count":323,"id":378236,"video":false,"vote_average":5.8,"title":"The Emoji Movie","popularity":45.596846,"poster_path":"/f5pF4OYzh4wb1dYL2ARQNdqUsEZ.jpg","original_language":"en","original_title":"The Emoji Movie","genre_ids":[35,10751,16],"backdrop_path":"/kTrgxhRSj2sun89bDbnGCPBiey6.jpg","adult":false,"overview":"Gene, a multi-expressional emoji, sets out on a journey to become a normal emoji.","release_date":"2017-07-28"},{"vote_count":171,"id":395834,"video":false,"vote_average":7.4,"title":"Wind River","popularity":43.899636,"poster_path":"/fzsaNUcRH0cV2gg5rjQH83ZEI8M.jpg","original_language":"en","original_title":"Wind River","genre_ids":[28,80,9648,53],"backdrop_path":"/iF9d73lbtDYeCsPhQmjtkEmlrYG.jpg","adult":false,"overview":"An FBI agent teams with the town's veteran game tracker to investigate a murder that occurred on a Native American reservation.","release_date":"2017-08-03"},{"vote_count":1027,"id":372058,"video":false,"vote_average":8.5,"title":"Your Name.","popularity":41.83737,"poster_path":"/xq1Ugd62d23K2knRUx6xxuALTZB.jpg","original_language":"ja","original_title":"君の名は。","genre_ids":[10749,16,18],"backdrop_path":"/mMtUybQ6hL24FXo0F3Z4j2KG7kZ.jpg","adult":false,"overview":"High schoolers Mitsuha and Taki are complete strangers living separate lives. But one night, they suddenly switch places. Mitsuha wakes up in Taki\u2019s body, and he in hers. This bizarre occurrence continues to happen randomly, and the two must adjust their lives around each other.","release_date":"2016-08-26"},{"vote_count":15,"id":290512,"video":false,"vote_average":7.2,"title":"The Mountain Between Us","popularity":41.632696,"poster_path":"/3XNfYTW4XGscI81nXMSWGsQ8cpu.jpg","original_language":"en","original_title":"The Mountain Between Us","genre_ids":[10749,12,18],"backdrop_path":"/gB2xvyQCsKmYhfKdcipyBjmDdsH.jpg","adult":false,"overview":"Stranded after a tragic plane crash, two strangers must forge a connection to survive the extreme elements of a remote snow covered mountain. When they realize help is not coming, they embark on a perilous journey across the wilderness.","release_date":"2017-10-05"},{"vote_count":0,"id":420622,"video":false,"vote_average":0,"title":"Professor Marston & the Wonder Women","popularity":40.225422,"poster_path":"/tbrzHlnE8dNpllLWEe9bwDGNzLe.jpg","original_language":"en","original_title":"Professor Marston & the Wonder Women","genre_ids":[36],"backdrop_path":"/ePJFR8T6oYonc5Nio4HYE2paa4m.jpg","adult":false,"overview":"The unconventional life of Dr. William Marston, the Harvard psychologist and inventor who helped invent the modern lie detector test and created Wonder Woman in 1941.","release_date":"2017-10-13"},{"vote_count":0,"id":284053,"video":false,"vote_average":0,"title":"Thor: Ragnarok","popularity":40.165403,"poster_path":"/avy7IR8UMlIIyE2BPCI4plW4Csc.jpg","original_language":"en","original_title":"Thor: Ragnarok","genre_ids":[28,12,14,878],"backdrop_path":"/wBzMnQ01R9w58W6ucltdYfOyP4j.jpg","adult":false,"overview":"Thor is imprisoned on the other side of the universe and finds himself in a race against time to get back to Asgard to stop Ragnarok, the destruction of his homeworld and the end of Asgardian civilization, at the hands of an all-powerful new threat, the ruthless Hela.","release_date":"2017-10-25"},{"vote_count":1,"id":407445,"video":false,"vote_average":0,"title":"Breathe","popularity":37.116527,"poster_path":"/8mnShwZHbQVT2NcF1ImhzTYLVaF.jpg","original_language":"en","original_title":"Breathe","genre_ids":[18],"backdrop_path":"/quVfDYktBYQfojgZ3E9pYrclGse.jpg","adult":false,"overview":"Based on the true story of Robin, a handsome, brilliant and adventurous man whose life takes a dramatic turn when polio leaves him paralyzed.","release_date":"2017-10-13"},{"vote_count":1,"id":274855,"video":false,"vote_average":0,"title":"Geostorm","popularity":37.042555,"poster_path":"/nrsx0jEaBgXq4PWo7SooSnYJTv.jpg","original_language":"en","original_title":"Geostorm","genre_ids":[28],"backdrop_path":"/lhTtnsPmEYUJB7nOaTKJzYoxJ7X.jpg","adult":false,"overview":"Gerard Butler playing a stubborn but charming satellite designer who, when the world\u2019s climate-controlling satellites malfunction, has to work together with his estranged brother to save the world from a man-made storm of epic proportions. A trip into space follows, while on Earth a plot to assassinate the president begins to unfold.","release_date":"2017-10-19"},{"vote_count":1,"id":423087,"video":false,"vote_average":10,"title":"6 Below: Miracle on the Mountain","popularity":35.978546,"poster_path":"/8eZynKoLiPq9b6DQGLOFHt6mZPv.jpg","original_language":"en","original_title":"6 Below: Miracle on the Mountain","genre_ids":[53,18],"backdrop_path":"/jW0bWpP5jcWnurm1RoBmJtRKE1f.jpg","adult":false,"overview":"An adrenaline seeking snowboarder gets lost in a massive winter storm in the back country of the High Sierras where he is pushed to the limits of human endurance and forced to battle his own personal demons as he fights for survival....","release_date":"2017-10-13"},{"vote_count":156,"id":378064,"video":false,"vote_average":8.1,"title":"A Silent Voice","popularity":33.617341,"poster_path":"/drlyoSKDOPnxzJFrRWGqzDsyJvR.jpg","original_language":"ja","original_title":"聲の形","genre_ids":[16,18],"backdrop_path":"/geINneHhMbGDeDCGun0elRjVQ1Z.jpg","adult":false,"overview":"Shouya Ishida starts bullying the new girl in class, Shouko Nishimiya, because she is deaf. But as the teasing continues, the rest of the class starts to turn on Shouya for his lack of compassion. When they leave elementary school, Shouko and Shouya do not speak to each other again... until an older, wiser Shouya, tormented by his past behaviour, decides he must see Shouko once more. He wants to atone for his sins, but is it already too late...?","release_date":"2016-09-17"},{"vote_count":1,"id":298250,"video":false,"vote_average":0,"title":"Jigsaw","popularity":33.597034,"poster_path":"/zUbUtxiTdEgWnkXY945gtYYqBZ1.jpg","original_language":"en","original_title":"Jigsaw","genre_ids":[27,53],"backdrop_path":"/ytKpFaLMpFWnuSXStz1GHrtTt6R.jpg","adult":false,"overview":"Dead bodies begin to turn up all over the city, each meeting their demise in a variety of grisly ways. All investigations begin to point the finger at deceased killer John Kramer.","release_date":"2017-10-20"},{"vote_count":0,"id":466876,"video":false,"vote_average":0,"title":"Gnome Alone","popularity":33.058874,"poster_path":"/qIRGy46IqxqlHkOjCSjyP2tFAtP.jpg","original_language":"en","original_title":"Gnome Alone","genre_ids":[10751,16],"backdrop_path":null,"adult":false,"overview":"When Chloe discovers that her new home's garden gnomes are not what they seem, she must decide between the pursuit of a desired high school life and taking up the fight against the Troggs.","release_date":"2017-10-13"},{"vote_count":1,"id":395991,"video":false,"vote_average":0,"title":"Only the Brave","popularity":30.142219,"poster_path":"/lC7WdUNLOJI3sllaDGNdFy2GT8g.jpg","original_language":"en","original_title":"Only the Brave","genre_ids":[18],"backdrop_path":"/oYuDWf2a42y7qu4XBdMV9Lioyk0.jpg","adult":false,"overview":"Members of the Granite Mountain Hotshots battle deadly wildfires to save an Arizona town.","release_date":"2017-10-19"},{"vote_count":14,"id":399057,"video":false,"vote_average":0,"title":"The Killing of a Sacred Deer","popularity":26.221622,"poster_path":"/lZ9I4Lv0QtdrykZ8pF42KJAdcg4.jpg","original_language":"en","original_title":"The Killing of a Sacred Deer","genre_ids":[53,27,18,9648],"backdrop_path":"/854uDv6rzbwF82jOtFe931SMrRs.jpg","adult":false,"overview":"A teenager's attempts to bring a brilliant surgeon into his dysfunctional family take an unexpected turn.","release_date":"2017-10-20"},{"vote_count":129,"id":399170,"video":false,"vote_average":6.4,"title":"Logan Lucky","popularity":25.359398,"poster_path":"/ksx6hvl5j04qgGeW0Y1ajkXx5KB.jpg","original_language":"en","original_title":"Logan Lucky","genre_ids":[28,80,35],"backdrop_path":"/vzu7tNNF5jT6u2AScPwi6qF7T75.jpg","adult":false,"overview":"Trying to reverse a family curse, brothers Jimmy and Clyde Logan set out to execute an elaborate robbery during the legendary Coca-Cola 600 race at the Charlotte Motor Speedway.","release_date":"2017-08-17"}]
     * page : 1
     * total_results : 284
     * dates : {"maximum":"2017-11-08","minimum":"2017-10-18"}
     * total_pages : 15
     */

    private int page;
    private int total_results;
    private DatesBean dates;
    private int total_pages;
    private List<ResultsBean> results;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public DatesBean getDates() {
        return dates;
    }

    public void setDates(DatesBean dates) {
        this.dates = dates;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public List<ResultsBean> getResults() {
        return results;
    }

    public void setResults(List<ResultsBean> results) {
        this.results = results;
    }

    public static class DatesBean {
        /**
         * maximum : 2017-11-08
         * minimum : 2017-10-18
         */

        private String maximum;
        private String minimum;

        public String getMaximum() {
            return maximum;
        }

        public void setMaximum(String maximum) {
            this.maximum = maximum;
        }

        public String getMinimum() {
            return minimum;
        }

        public void setMinimum(String minimum) {
            this.minimum = minimum;
        }
    }

    public static class ResultsBean {
        /**
         * vote_count : 555
         * id : 335984
         * video : false
         * vote_average : 7.8
         * title : Blade Runner 2049
         * popularity : 379.475381
         * poster_path : /cbRQVCia0urtv5UGsVFTdqLDIRv.jpg
         * original_language : en
         * original_title : Blade Runner 2049
         * genre_ids : [28,9648,878,53]
         * backdrop_path : /mVr0UiqyltcfqxbAUcLl9zWL8ah.jpg
         * adult : false
         * overview : Thirty years after the events of the first film, a new blade runner, LAPD Officer K, unearths a long-buried secret that has the potential to plunge what's left of society into chaos. K's discovery leads him on a quest to find Rick Deckard, a former LAPD blade runner who has been missing for 30 years.
         * release_date : 2017-10-04
         */

        private int vote_count;
        private int id;
        private boolean video;
        private double vote_average;
        private String title;
        private double popularity;
        private String poster_path;
        private String original_language;
        private String original_title;
        private String backdrop_path;
        private boolean adult;
        private String overview;
        private String release_date;
        private List<Integer> genre_ids;

        public int getVote_count() {
            return vote_count;
        }

        public void setVote_count(int vote_count) {
            this.vote_count = vote_count;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public boolean isVideo() {
            return video;
        }

        public void setVideo(boolean video) {
            this.video = video;
        }

        public double getVote_average() {
            return vote_average;
        }

        public void setVote_average(double vote_average) {
            this.vote_average = vote_average;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public double getPopularity() {
            return popularity;
        }

        public void setPopularity(double popularity) {
            this.popularity = popularity;
        }

        public String getPoster_path() {
            return poster_path;
        }

        public void setPoster_path(String poster_path) {
            this.poster_path = poster_path;
        }

        public String getOriginal_language() {
            return original_language;
        }

        public void setOriginal_language(String original_language) {
            this.original_language = original_language;
        }

        public String getOriginal_title() {
            return original_title;
        }

        public void setOriginal_title(String original_title) {
            this.original_title = original_title;
        }

        public String getBackdrop_path() {
            return backdrop_path;
        }

        public void setBackdrop_path(String backdrop_path) {
            this.backdrop_path = backdrop_path;
        }

        public boolean isAdult() {
            return adult;
        }

        public void setAdult(boolean adult) {
            this.adult = adult;
        }

        public String getOverview() {
            return overview;
        }

        public void setOverview(String overview) {
            this.overview = overview;
        }

        public String getRelease_date() {
            return release_date;
        }

        public void setRelease_date(String release_date) {
            this.release_date = release_date;
        }

        public List<Integer> getGenre_ids() {
            return genre_ids;
        }

        public void setGenre_ids(List<Integer> genre_ids) {
            this.genre_ids = genre_ids;
        }
    }
}
